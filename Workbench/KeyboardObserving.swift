//
//  KeyboardObserving.swift
//  Workbench
//
//  Workbench is a set of tools that I have built and collected to make working on apps simpler.
//
//  Unless otherwise indicated, this work is licensed under a Creative Commons Attribution 4.0 International License.
//  https://creativecommons.org/licenses/by/4.0/
//  Brent Marykuca, 2016

import Foundation
import UIKit

@objc public protocol KeyboardObserving: NSObjectProtocol
{
    @objc optional func keyboardWillShow(_ note: Notification)
    @objc optional func keyboardWillHide(_ note: Notification)
}

public extension KeyboardObserving
{
    func registerForKeyboardNotifications()
    {
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self, selector: #selector(KeyboardObserving.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(KeyboardObserving.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}

public struct KeyboardObserverInfo
{
    let info: [AnyHashable: Any]
    
    public init(note: Notification)
    {
        info = (note as NSNotification).userInfo!
    }
    
    public var frameBegin: CGRect
    {
        return (info[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue
    }
    
    public var frameEnd: CGRect
    {
        return (info[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue
    }
}
