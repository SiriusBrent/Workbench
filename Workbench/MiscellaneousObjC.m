//
//  ObjC.m
//  Workbench
//
//  Workbench is a set of tools that I have built and collected to make working on apps simpler.
//
//  Unless otherwise indicated, this work is licensed under a Creative Commons Attribution 4.0 International License.
//  https://creativecommons.org/licenses/by/4.0/
//  Brent Marykuca, 2016

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// Collected at:
// http://stackoverflow.com/questions/23512700/how-to-set-text-color-of-status-bar-other-than-white-and-black?noredirect=1&lq=1
/// Sets the status bar text color. Returns YES on success.
/// Works in iOS 7-10. Uses undocumented SPI.
///
BOOL setStatusBarColor(UIColor *color)
{
    id statusBarWindow = [[UIApplication sharedApplication] valueForKey:@"statusBarWindow"];
    id statusBar = [statusBarWindow valueForKey:@"statusBar"];

    SEL setForegroundColor_sel = NSSelectorFromString(@"setForegroundColor:");
    if ([statusBar respondsToSelector:setForegroundColor_sel])
    {
        // iOS 7+
        // This just does [statusBar performSelector: setForegroundColor_set withObject: color]
        // but does so without a warning about a potential leak.
        NSInvocation* const invocation = [NSInvocation invocationWithMethodSignature:[[statusBar class] instanceMethodSignatureForSelector:setForegroundColor_sel]];
        invocation.target = statusBar;
        invocation.selector = setForegroundColor_sel;
        [invocation setArgument:&color atIndex:2];
        [invocation invoke];
        return YES;
    }
    else
    {
        return NO;
    }
}
