//
//  Miscellaneous.swift
//  Workbench
//
//  Workbench is a set of tools that I have built and collected to make working on apps simpler.
//
//  Unless otherwise indicated, this work is licensed under a Creative Commons Attribution 4.0 International License.
//  https://creativecommons.org/licenses/by/4.0/
//  Brent Marykuca, 2016

import Foundation
import UIKit

public func runLaterOnMainQueue(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

public func runOnMainQueue(_ closure:@escaping ()->())
{
    DispatchQueue.main.async(execute: closure)
}

public func FooFunction()
{
    print("foo")
}

