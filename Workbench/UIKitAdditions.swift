//
//  UIKitAdditions.swift
//  Workbench
//
//  Workbench is a set of tools that I have built and collected to make working on apps simpler.
//
//  Unless otherwise indicated, this work is licensed under a Creative Commons Attribution 4.0 International License.
//  https://creativecommons.org/licenses/by/4.0/
//  Brent Marykuca, 2016

import Foundation
import UIKit

// UIButton

public extension UIButton
{
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
        }
    }
    @IBInspectable var borderColor: UIColor! {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue.cgColor
        }
    }
}

// UIColor

public extension UIColor
{
    convenience init(hexColor:Int)
    {
        let red   = CGFloat((hexColor & 0x00ff0000) >> 16) / 255
        let green = CGFloat((hexColor & 0x0000ff00) >>  8) / 255
        let blue  = CGFloat(hexColor & 0x000000ff) / 255
        
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
}

// UIView

public extension UIView
{
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = self.layer.cornerRadius > 0
        }
    }
}

