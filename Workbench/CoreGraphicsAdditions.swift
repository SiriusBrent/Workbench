//
//  CoreGraphicsAdditions.swift
//  Workbench
//
//  Workbench is a set of tools that I have built and collected to make working on apps simpler.
//
//  Unless otherwise indicated, this work is licensed under a Creative Commons Attribution 4.0 International License.
//  https://creativecommons.org/licenses/by/4.0/
//  Brent Marykuca, 2016

import Foundation
import CoreGraphics
import UIKit

// Mark CGSize

public extension CGSize
{
    init(square size: Int)
    {
        self.init(width: size, height: size)
    }
    
    func sizeByChangingHeight(_ newHeight: CGFloat) -> CGSize
    {
        return CGSize(width: self.width, height: newHeight)
    }
    
    var aspectRatio: CGFloat
    {
        return self.width / self.height
    }
}

public func / (a: CGSize, b: CGFloat) -> CGSize
{
    return CGSize(width: a.width / b, height: a.height / b)
}

public func * (a: CGSize, b: CGFloat) -> CGSize
{
    return CGSize(width: a.width * b, height: a.height * b)
}

// CGRect

public extension CGRect
{
    init(size: CGSize) // convenience for initializing at origin.
    {
        self.init(origin: CGPoint.zero, size: size)
    }
    
    var leftEdge: CGFloat
    {
        return self.origin.x
    }
    var centerX: CGFloat
    {
        return self.origin.x + (self.width / 2)
    }
    var rightEdge: CGFloat
    {
        return self.origin.x + self.width
    }
    var topEdge: CGFloat
    {
        return self.origin.y
    }
    var bottomEdge: CGFloat
    {
        return self.origin.y + self.height
    }
    var bottomCenter: CGPoint
    {
        return CGPoint(x: self.centerX, y: self.bottomEdge)
    }
    
    func rectByChangingHeight(_ newHeight: CGFloat) -> CGRect
    {
        return CGRect(origin: self.origin, size: self.size.sizeByChangingHeight(newHeight))
    }
    func rectByOffsettingWithPoint(_ offsetPoint: CGPoint) -> CGRect
    {
        return CGRect(origin: CGPoint(x: self.origin.x + offsetPoint.x, y: self.origin.y + offsetPoint.y), size: self.size)
    }
    func rectBySettingBottomCenterToPoint(_ newBottomCenter: CGPoint) -> CGRect
    {
        return self.rectByOffsettingWithPoint(newBottomCenter - self.bottomCenter)
    }
}


public func -(a:CGPoint, b:CGPoint) -> CGPoint
{
    return CGPoint(x: a.x - b.x, y: a.y - b.y)
}

public func +(a:CGPoint, b:CGSize) -> CGPoint
{
	return CGPoint(x: a.x + b.width, y: a.y + b.height)
}

public func +(a:CGPoint, d:(CGFloat, CGFloat)) -> CGPoint
{
	return CGPoint(x: a.x + d.0, y: a.y + d.1)
}

public func CGPointDistance(_ p1: CGPoint, _ p2: CGPoint) -> CGFloat
{
    let d1 = p1.x - p2.x
    let d2 = p1.y - p2.y
    return sqrt(d1 * d1 + d2 * d2);
}



public func GetGrayCGColor(_ grayAmount: CGFloat) -> CGColor?
{
    return CGColor(colorSpace: CGColorSpaceCreateDeviceGray(), components: [grayAmount, 1.0])
}

public func DrawUIImageWithSize(_ imageSize: CGSize, drawingCode: (CGContext, CGSize) -> ()) -> UIImage!
{
    UIGraphicsBeginImageContext(imageSize)
    defer {
        UIGraphicsEndImageContext()
    }
    
    if let context = UIGraphicsGetCurrentContext()
    {
        drawingCode(context, imageSize)
    }
    return UIGraphicsGetImageFromCurrentImageContext()
}

public func DrawUIImageWithSize(_ imageSize: CGSize, scale: CGFloat, drawingCode: (CGContext, CGSize) -> ()) -> UIImage!
{
    UIGraphicsBeginImageContextWithOptions(imageSize, false, scale)
    defer {
        UIGraphicsEndImageContext()
    }
    
    if let context = UIGraphicsGetCurrentContext()
    {
        drawingCode(context, imageSize)
    }
    return UIGraphicsGetImageFromCurrentImageContext()
}

