# Workbench

## Summary

Some tools to make working on apps simpler.

## Description

*Workbench* is a set of tools that I have built and collected to make working on apps simpler. It mostly consists of extensions to iOS classes that provide functionality that I wish had been included by Apple.

## License

Unless otherwise indicated, this work is licensed under a Creative Commons Attribution 4.0 International License.

https://creativecommons.org/licenses/by/4.0/

## Author

Brent Marykuca


