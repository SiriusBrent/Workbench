//
//  MiscellaneousObjC.h
//  Workbench
//
//  Workbench is a set of tools that I have built and collected to make working on apps simpler.
//
//  Unless otherwise indicated, this work is licensed under a Creative Commons Attribution 4.0 International License.
//  https://creativecommons.org/licenses/by/4.0/
//  Brent Marykuca, 2016

#pragma once
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

BOOL setStatusBarColor(UIColor *color);

